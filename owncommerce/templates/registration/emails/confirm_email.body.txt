{% load i18n %}Saleor

{% block content %}{% blocktrans %}Hi!

You have requested login access to saleor project. To continue visit the link below.

{{ confirmation_url }}

If you didn't try to login to saleor please ignore this message. We apologise for inconvenience.

Sincerely,
SITE_NAME{% endblocktrans %}{% endblock %}
