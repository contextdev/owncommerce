from django.utils.translation import ugettext_lazy as _
from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar
from cms.utils.urlutils import admin_reverse
from .models import CustomCss


@toolbar_pool.register
class PollToolbar(CMSToolbar):
    watch_models = [CustomCss,]

    def populate(self):
        menu = self.toolbar.get_or_create_menu('custom-css', _('Custom Css'))
        menu.add_sideframe_item(
            name=_('Custom Css list'),
            url=admin_reverse('custom_theme_customcss_changelist'),
            # url=admin_reverse('custom_css_customcss_changelist'),
        )
        menu.add_modal_item(
            name=_('Add new Custom Css'),
            url=admin_reverse('custom_theme_customcss_add'),
            # url=admin_reverse('custom_css_customcss_add'),
        )