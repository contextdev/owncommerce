from django.views.generic import ListView
from saleor.product.models import Product, Category, ProductVariant
from django.db.models import Q
from .serializers import ProductSerializer, ProductVariantSerializer, ProductVariantSerializer1
from rest_framework import viewsets

from django.contrib import messages

from django.core.cache import cache
from django.utils.cache import get_cache_key
from django.http.response import JsonResponse


class ProductSearch(ListView):
    model = Product
    queryset = Product.objects.all()
    context_object_name = 'products'
    template_name = 'product/search.html'
    paginate_by = 6

    def get_queryset(self):
        return self.get_cache_queryset()

    def get_cache_queryset(self):


        key = str(get_cache_key(self.request))
        query = cache.get(key)

        if query is None:
            query = self.queryset
            if self.request.GET.get('q'):
                query = query.filter(name__contains=self.request.GET.get('q'))

            if self.request.GET.get('category'):
                try:
                    cat = Category.objects.get(slug=self.request.GET.get('category'))
                    query= query.filter(Q(categories__slug=self.request.GET.get('category')) | Q(categories__in=cat.children.all()))
                except Category.DoesNotExist:
                    messages.add_message(self.request, messages.WARNING, 'Warning!! error with category %s' %(self.request.GET.get('category')))

            if self.request.GET.get('min_price') and self.request.GET.get('max_price'):
                query = query.filter(Q(price__gte = self.request.GET.get('min_price')) & Q(price__lte= self.request.GET.get('max_price')))

            if self.request.GET.get('order_by'):
                query = query.order_by(self.request.GET.get('order_by'))

            query = query.prefetch_related('images', 'variants', 'variants__stock')

            cache.set(key, query, 86400)

        return query

    def get_context_data(self, **kwargs):
        context = super(ProductSearch, self).get_context_data(**kwargs)
        context['min_price_param'] = self.request.GET.get('min_price', 0)
        context['max_price_param'] = self.request.GET.get('max_price', 10000)
        context['order_by_param'] = self.request.GET.get('order_by', 'Position')

        context['style_product'] = self.request.GET.get('style', 'column')

        # TODO: Quiet this line and create dynamic widget
        context['featured_products'] = Product.objects.filter(outstanding=True).prefetch_related('images')[:8]

        context['text_search'] = self.request.GET.get('q', '')
        try:
            context['category_selected'] = Category.objects.get(slug=self.request.GET.get('category', ''))
        except:
            context['category_selected'] = {}
        context['categories'] = Category.objects.filter(hidden=False, parent=None)
        return context

class ProductViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing Products
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class ProductVariantSet(viewsets.ModelViewSet):
    """"
    A simple ViewSet for viewwing Variants
    """
    queryset = ProductVariant.objects.all()
    serializer_class = ProductVariantSerializer


def product_variants_list(request, **kwargs):
    """
        Get JSON LIST to variants products
    """
    if kwargs.get('product_id'):
            variants = ProductVariant.objects.filter(product=kwargs['product_id'])
    else:
        variants = []

    return JsonResponse(ProductVariantSerializer1(variants, many=True).data, safe=False)




