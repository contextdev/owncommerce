from django import template
register = template.Library()
from ..models import CustomCss


@register.simple_tag(takes_context=True)
def get_custom_css(context):
    response = '<style id=custom-css>'

    for custom_css in CustomCss.objects.all().prefetch_related('attributes'):
        response += custom_css.selector + '{ '
        for attr in custom_css.attributes.all():
            response += attr.attr_name + ': ' + attr.value + ';'
        response += '}'
    response += '</style>'
    return response

