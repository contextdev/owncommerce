from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from owncommerce.settings.base import THEMES
# Create your views here.


def theme_config(request):
    theme = THEMES[0]

    for get_key, get_value in request.GET.items():
        if theme['config'].get(get_key):
            if any(get_value == opt for opt in theme['config'][get_key]['options']):
                request.session[get_key] = get_value

    return HttpResponseRedirect('/')