from django.contrib import admin
from . import models
from django.forms import ModelForm
from django_admin_dialog.mixins import DjangoAdminDialogMixin


class CustomSitePhoneInline(admin.TabularInline):
    model = models.CustomSitePhone
    form = ModelForm
    extra = 1


class CustomSiteEmailInline(admin.TabularInline):
    model = models.CustomSiteEmail
    form = ModelForm
    extra = 1


class CustomSiteEmailAccountInline(admin.TabularInline):
    template = 'admin/custom_site/tabular2.html'
    model = models.CustomSiteEmailAccount
    form = ModelForm
    extra = 0


class CustomSiteAdmin(DjangoAdminDialogMixin, admin.ModelAdmin):
    list_display = ('name',)
    inlines = [CustomSiteEmailInline, CustomSitePhoneInline, CustomSiteEmailAccountInline]

    def render_change_form(self, request, context, *args, **kwargs):
        extra = {
            'help_text': "Good luck Creating your Ecommerce!"
        }
        context.update(extra)
        return super(CustomSiteAdmin, self).render_change_form(request, context, *args, **kwargs)


admin.site.register(models.CustomSite, CustomSiteAdmin)


