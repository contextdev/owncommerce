from rest_framework import serializers
from . import models


class CustomSiteSerializer(serializers.ModelSerializer):
    emails = serializers.SerializerMethodField()
    phones = serializers.SerializerMethodField()

    class Meta:
        model = models.CustomSite

    def get_emails(self, obj):
        return list(obj.emails.all().values_list('email', flat=True))

    def get_phones(self, obj):
        return list(obj.phones.all().values_list('phone_number', flat=True))
