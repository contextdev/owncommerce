from django import template

register = template.Library()

from saleor.product.models import Product



@register.filter
def product_image_url(value):
    # TODO: performance?
    try:
        return Product.objects.get(id=value).images.first().image.url
    except:
        return '/static/image/product-default.png'
