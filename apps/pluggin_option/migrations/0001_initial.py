# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0012_auto_20150607_2207'),
        ('option', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OptionPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, serialize=False, to='cms.CMSPlugin', parent_link=True, primary_key=True)),
                ('layout', models.CharField(choices=[('Image', 'Images'), ('Text', 'Text')], max_length=10, default='Image')),
                ('option_set', models.ForeignKey(to='option.OptionSet', related_name='plugins')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
