from rest_framework import serializers
from saleor.product.models import Product, ProductVariant
from saleor.product.models import AttributeChoiceValue
from apps.option.models import OptionSet, Option


class ProductSerializer(serializers.ModelSerializer):
    options = serializers.SerializerMethodField()

    class Meta:
        model = Product
        exclude = ['price']

    def get_options(self, obj):
        option_setList = OptionSet.objects.all().filter(category__in=obj.categories.all())
        result = []
        for option_set in option_setList:
            options = []
            options_list = Option.objects.all().filter(parent__id=option_set.id)
            for option in options_list:
                options.append({
                    'id': option.id,
                    'parent_id': option.parent.id,
                    'title': option.title,
                    'description': option.description,
                    # TODO: Implement serializer to return different sizes of image
                    'image': {'url': option.image.url} if option.image else {}
                })
            result.append({
                'id': option_set.id,
                'title': option_set.title,
                'description': option_set.description,
                # TODO: Implement serializer to return different sizes of image
                'image': {'url': option_set.image.url} if option_set.image else {},
                'required': option_set.required,
                'options': options
            })
        return result


class ProductVariantSerializer1(serializers.ModelSerializer):

    class Meta:
        model = ProductVariant
        fields = ('id', 'name')


class ProductVariantSerializer(serializers.ModelSerializer):
    stock = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()
    attributes = serializers.SerializerMethodField()
    parent = ProductSerializer(read_only=True, source='product')

    class Meta:
        model = ProductVariant
        exclude = ['price_override']

    def get_stock(self, obj):
        return obj.get_stock_quantity()

    def get_price(self, obj):
        if not obj.price_override:
            return obj.product.price
        else:
            return obj.price_override

    def get_attributes(self, obj):
        attributes = []
        for (key,value) in obj.attributes.items():
            attribute_value = AttributeChoiceValue.objects.get(id=value)
            attr = {
                'id': attribute_value.attribute.id,
                'widget_type': attribute_value.attribute.widget_type,
                'name': attribute_value.attribute.name,
                'display_name': attribute_value.attribute.display,
                'value':attribute_value.pk,
                'color':attribute_value.color,
                'display_value': attribute_value.display,
            }

            attributes.append(attr)
        return attributes
