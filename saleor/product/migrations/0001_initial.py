# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
from decimal import Decimal
import satchless.item
import versatileimagefield.fields
import saleor.product.models.fields
import django_prices.models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AttributeChoiceValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('display', models.CharField(verbose_name='display name', max_length=100)),
                ('color', models.CharField(blank=True, verbose_name='color', validators=[django.core.validators.RegexValidator('^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$')], max_length=7)),
                ('image', versatileimagefield.fields.VersatileImageField(null=True, verbose_name='image', upload_to='attributes', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='name', max_length=128)),
                ('slug', models.SlugField(verbose_name='slug')),
                ('description', models.TextField(verbose_name='description', blank=True)),
                ('hidden', models.BooleanField(verbose_name='hidden', default=False)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', models.ForeignKey(null=True, verbose_name='parent', to='product.Category', blank=True, related_name='children')),
            ],
            options={
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='FixedProductDiscount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('discount', django_prices.models.PriceField(verbose_name='discount value', max_digits=12, currency='$', decimal_places=2)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='name', max_length=128)),
                ('description', models.TextField(verbose_name='description')),
                ('price', django_prices.models.PriceField(verbose_name='price', max_digits=12, currency='$', decimal_places=2)),
                ('weight', saleor.product.models.fields.WeightField(verbose_name='weight', max_digits=6, decimal_places=2, unit='lb')),
                ('available_on', models.DateField(null=True, verbose_name='available on', blank=True)),
                ('outstanding', models.BooleanField(default=False)),
            ],
            bases=(models.Model, satchless.item.ItemRange),
        ),
        migrations.CreateModel(
            name='ProductAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.SlugField(unique=True, verbose_name='internal name')),
                ('display', models.CharField(verbose_name='display name', max_length=100)),
                ('widget_type', models.CharField(choices=[('TXT', 'Text'), ('IMG', 'Image'), ('COL', 'Color')], max_length=3, default='TXT')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('image', versatileimagefield.fields.VersatileImageField(upload_to='products')),
                ('ppoi', versatileimagefield.fields.PPOIField(editable=False, max_length=20, default='0.5x0.5')),
                ('alt', models.CharField(verbose_name='short description', max_length=128, blank=True)),
                ('order', models.PositiveIntegerField(editable=False)),
                ('product', models.ForeignKey(to='product.Product', related_name='images')),
            ],
            options={
                'ordering': ['order'],
            },
        ),
        migrations.CreateModel(
            name='ProductVariant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('sku', models.CharField(unique=True, verbose_name='SKU', max_length=32)),
                ('name', models.CharField(verbose_name='variant name', max_length=100, blank=True)),
                ('price_override', django_prices.models.PriceField(null=True, verbose_name='price override', max_digits=12, decimal_places=2, blank=True, currency='$')),
                ('weight_override', saleor.product.models.fields.WeightField(null=True, verbose_name='weight override', max_digits=6, decimal_places=2, unit='lb', blank=True)),
                ('attributes', jsonfield.fields.JSONField(verbose_name='attributes', default={})),
                ('product', models.ForeignKey(to='product.Product', related_name='variants')),
            ],
            bases=(models.Model, satchless.item.Item),
        ),
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('location', models.CharField(verbose_name='location', max_length=100)),
                ('quantity', models.IntegerField(verbose_name='quantity', validators=[django.core.validators.MinValueValidator(0)], default=Decimal('1'))),
                ('reserved', models.IntegerField(verbose_name='quantity_reserved', validators=[django.core.validators.MinValueValidator(0)], default=Decimal('0'))),
                ('cost_price', django_prices.models.PriceField(null=True, verbose_name='cost price', max_digits=12, decimal_places=2, blank=True, currency='$')),
                ('variant', models.ForeignKey(unique=True, verbose_name='variant', to='product.ProductVariant', related_name='stock')),
            ],
        ),
        migrations.AddField(
            model_name='product',
            name='attributes',
            field=models.ManyToManyField(to='product.ProductAttribute', blank=True, related_name='products'),
        ),
        migrations.AddField(
            model_name='product',
            name='categories',
            field=models.ManyToManyField(to='product.Category', verbose_name='categories', related_name='products'),
        ),
        migrations.AddField(
            model_name='fixedproductdiscount',
            name='products',
            field=models.ManyToManyField(to='product.Product', blank=True),
        ),
        migrations.AddField(
            model_name='attributechoicevalue',
            name='attribute',
            field=models.ForeignKey(to='product.ProductAttribute', related_name='values'),
        ),
        migrations.AlterUniqueTogether(
            name='stock',
            unique_together=set([('variant', 'location')]),
        ),
    ]
