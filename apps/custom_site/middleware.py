from .models import CustomSite, CustomSitePhone
from .serializers import CustomSiteSerializer
import json

# assuming obj is a model instance
# serialized_obj = serializers.serialize('json', [ obj, ])


class CustomSiteMiddleware(object):

    def process_request(self, request):
        site = CustomSite.objects.all().first()
        if site:
            request.session['site_logo'] = site.logo.url
            request.session['site_name'] = site.name
            request.session['site_display_name'] = site.display_name
            request.session['site_url'] = site.site_url
            request.session['site_slogan'] = site.slogan
            request.session['site_contact_email'] = site.contact_email
            request.session['site_contact_email'] = site.contact_email
            request.session['site_skype'] = site.skype

            phones = []
            for phone in site.phones.all():
                phones.append(str(phone))
            request.session['site_contact_phones'] = phones

            emails = []
            for email in site.emails.all():
                emails.append(str(email))
            request.session['site_contact_emails'] = emails

            data = CustomSiteSerializer(site).data
            # struct = json.loads(data)
            # data = json.dumps(struct[0])
            #
            request.session['custom_site'] = data
            print(20*"=")
            print(data)
            # request.session['site_contact_emails'] = serializers.serialize('json', [site,])[0]


