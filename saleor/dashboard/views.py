from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required \
    as _staff_member_required
from django.db.models import Q, Sum, F
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormMixin

from ..order.models import Order, Payment
from ..product.models import Product, ProductVariant
from saleor.dashboard.order.forms import OrderFilterForm
from saleor.dashboard.utils import paginate


def staff_member_required(f):
    return _staff_member_required(f, login_url='registration:login')


class StaffMemberOnlyMixin(object):
    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(StaffMemberOnlyMixin, self).dispatch(*args, **kwargs)


class FilterByStatusMixin(FormMixin):
    form_class = OrderFilterForm

    def __init__(self):
        super(FilterByStatusMixin, self).__init__()
        self.active_filter = None

    def get_queryset(self):
        queryset = super(FilterByStatusMixin, self).get_queryset()
        active_filter = self.request.GET.get('status')
        if active_filter:
            self.active_filter = active_filter
            queryset = queryset.filter(status=self.active_filter)
        return queryset

    def get_initial(self):
        initial = super(FilterByStatusMixin, self).get_initial()
        if self.active_filter:
            initial['status'] = self.active_filter
        return initial

    def get_context_data(self):
        ctx = super(FilterByStatusMixin, self).get_context_data()
        ctx['form'] = self.get_form()
        return ctx


@staff_member_required
def index(request):
    orders_to_ship = Order.objects.filter(status='fully-paid')
    orders_to_ship = (orders_to_ship
                      .select_related('user')
                      .prefetch_related('groups', 'groups__items', 'payments'))
    payments = Payment.objects.filter(status='preauth').order_by('-created')
    payments = payments.select_related('order', 'order__user')

    low_stock, low_stock_paginator = paginate(get_low_stock_productVariants(), 5, request.GET.get('page'))

    ctx = {'preauthorized_payments': payments, 'orders_to_ship': orders_to_ship,
           'low_stock': low_stock, 'low_stock_paginator': low_stock_paginator}
    return TemplateResponse(request, 'dashboard/index.html', ctx)


def get_low_stock_productVariants():
    threshold = getattr(settings, 'LOW_STOCK_THRESHOLD', 100)
    variants = ProductVariant.objects.all().filter(stock__quantity__lt=F('stock__reserved') + threshold).prefetch_related('stock')
    return variants
