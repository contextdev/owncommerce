from django.contrib import admin

from .models import OptionSet, Option


class OptionInline(admin.TabularInline):
    model = Option
    extra = 1
    fk_name = 'parent'

class OptionSetAdmin(admin.ModelAdmin):
    inlines = (OptionInline, )

    def get_queryset(self, request):
        return OptionSet.objects.all()


admin.site.register(OptionSet, OptionSetAdmin)
