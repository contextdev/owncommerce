from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from .models import (ProductImage, Category, FixedProductDiscount, Product, ProductVariant, ProductAttribute, Stock, InputOutputStock)
from .forms import ImageInline


class ImageAdminInline(admin.StackedInline):
    model = ProductImage
    formset = ImageInline


class ProductCollectionAdmin(admin.ModelAdmin):
    search_fields = ['name']

class StockAdmin(admin.ModelAdmin):
    list_display = ('id', 'variant', 'location', 'quantity', 'reserved', 'cost_price',)

class ProductVariantAdmin(admin.ModelAdmin):
    list_display = ('id', 'sku', 'name', 'product')


admin.site.register(InputOutputStock, admin.ModelAdmin)

admin.site.register(Category, MPTTModelAdmin)
admin.site.register(FixedProductDiscount)

admin.site.register(Product, admin.ModelAdmin)
admin.site.register(ProductVariant, ProductVariantAdmin)
admin.site.register(ProductAttribute, admin.ModelAdmin)
admin.site.register(Stock, StockAdmin)
