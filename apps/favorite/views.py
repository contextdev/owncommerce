from django.views.generic import ListView
from saleor.product.models import Product
from favit.models import Favorite
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


class UserFavoritesListView(ListView):
    model = Product
    template_name = 'userprofile/favorites.html'
    context_object_name = 'products'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(UserFavoritesListView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return Product.objects.filter(id__in=Favorite.objects.for_user(self.request.user, model=Product).values_list('target_object_id'))


