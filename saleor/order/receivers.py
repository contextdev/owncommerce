from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save

from .signals import post_order_change_status, post_delivery_group_change_status
from .models import Order, DeliveryGroup, OrderedItem, Stock


@receiver(post_order_change_status, sender=Order, dispatch_uid="update_delivery_group_on_order_change_status")
def update_delivery_group_on_order_change_status(sender, *args, **kwargs):
    old_status = kwargs['old_status']
    new_status = kwargs['new_status']
    order = kwargs['order']

    if old_status != 'cancelled' and new_status == 'cancelled':
        for group in DeliveryGroup.objects.all().filter(order=order):
            group.change_status('cancelled')
    elif old_status == 'new' and new_status == 'processing':
        for group in DeliveryGroup.objects.all().filter(order=order):
            group.change_status('processing')


# quizas deberia ejecutar cuando  se actualiza OrderedItem
@receiver(post_delivery_group_change_status, sender=DeliveryGroup, dispatch_uid="update_stock_on_delivery_group_change_status")
def update_stock_on_delivery_group_change_status(sender, *args, **kwargs):
    old_status = kwargs['old_status']
    new_status = kwargs['new_status']
    order = kwargs['instance'].order

    if old_status == 'new' and new_status == 'processing':
        # Reserve Stock
        for item in order.get_items():
            stock = Stock.objects.get(variant__sku=item.product_sku)
            stock.reserve(item.quantity)
    elif old_status != 'cancelled' and old_status != 'shipped' and new_status == 'cancelled':
        # Revert Stock Reservations
        for item in order.get_items():
            stock = Stock.objects.get(variant__sku=item.product_sku)
            stock.reserve(-item.quantity)
    elif old_status != 'shipped' and new_status == 'shipped':
        # Revert Stock Reservations
        for item in order.get_items():
            stock = Stock.objects.get(variant__sku=item.product_sku)
            stock.consume(item.quantity)

