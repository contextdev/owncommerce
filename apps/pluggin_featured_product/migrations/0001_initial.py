# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0012_auto_20150607_2207'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeaturedProductPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, serialize=False, to='cms.CMSPlugin', parent_link=True, primary_key=True)),
                ('title', models.CharField(max_length=40)),
                ('description', models.TextField()),
                ('layout', models.CharField(choices=[('h', 'Horizontal'), ('v', 'Vertical')], max_length=10, default='h')),
                ('query', models.CharField(choices=[('favorite', 'Favorites'), ('seller', 'Seller'), ('featured', 'Featured')], max_length=40)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
