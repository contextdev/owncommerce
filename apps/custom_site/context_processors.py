from .models import CustomSite
from .serializers import CustomSiteSerializer
from django.conf import settings


# TODO: Cache this function
def custom_site_preprocessor(request):
    site = CustomSite.objects.all().first()
    site_data = settings.SITE_DEFAULT_DATA
    if site:
        site_data = CustomSiteSerializer(site).data

    return {'custom_site': site_data}