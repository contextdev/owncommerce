from django.views.generic import TemplateView

from saleor.product.models import Product
from favit.models import Favorite
from django.db.models import Count


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        # Get top 8 products with more favorites
        objs_fav = Favorite.objects.for_model(Product).annotate(num_fav=Count('target_object_id')).order_by('-num_fav').values_list('target_object_id')[:8]
        context['favorites_p'] = Product.objects.filter(id__in=objs_fav).prefetch_related('images')
        context['featured_p'] = Product.objects.all().filter(outstanding=True).prefetch_related('images')[:6]
        return context


