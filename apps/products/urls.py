from django.conf.urls import url, include, patterns
from .views import ProductSearch
from saleor.product import views

urlpatterns = patterns('',
                       url(r'^$', ProductSearch.as_view(), name='search'),
                       url(r'^(?P<product_id>[0-9]+)/variations/$',
                           'apps.products.views.product_variants_list', name='variations'),
                       url(r'^(?P<slug>[a-z0-9-]+?)-(?P<product_id>[0-9]+)/$',
                           views.product_details, name='details'),
                       url(r'^category/(?P<path>[a-z0-9-_/]+?)-(?P<category_id>[0-9]+)/$',
                           views.category_index, name='category')
)