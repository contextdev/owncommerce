from __future__ import unicode_literals

from django import forms
from django.forms import widgets
from django.forms.models import inlineformset_factory
from django.utils.translation import pgettext_lazy

from ...product.models import (ProductImage, Stock, ProductVariant, Product,
                               ProductAttribute, AttributeChoiceValue, InputOutputStock)
from .widgets import ImagePreviewWidget

PRODUCT_CLASSES = {Product: 'Default'}


class ProductClassForm(forms.Form):
    product_cls = forms.ChoiceField(
        label=pgettext_lazy('Product class form label', 'Product class'),
        widget=forms.RadioSelect,
        choices=[(cls.__name__, presentation) for cls, presentation in
                 PRODUCT_CLASSES.items()])

    def __init__(self, *args, **kwargs):
        super(ProductClassForm, self).__init__(*args, **kwargs)
        product_class = next(iter((PRODUCT_CLASSES)))
        self.fields['product_cls'].initial = product_class.__name__


from django_select2.fields import Select2Widget
import json


class Select2ProductWidget(Select2Widget):

    def render_js_script(self, inner_code):
        return """
                <script type="text/javascript">
                    $(document).ready(function() {
                        jQuery(function ($) {
                            %s
                        });
                    });
                </script>
                """ % inner_code

    def render_inner_js_code(self, id_, *args):
        self.options['closeOnSelect'] = True
        options = json.dumps(self.get_options())
        options = options.replace('"*START*', '').replace('*END*"', '')
        js = 'var hashedSelector = "#" + "%s";' % id_
        js += '$(hashedSelector).select2(%s).trigger("change").change(changeProductSelected);' % (options)
        return js



class InputOutputStockForm(forms.ModelForm):
    product = forms.ModelChoiceField(queryset=Product.objects.all(),
                                     widget=Select2ProductWidget(attrs={'class': 'browser-default select-product'}))

    variation_hidden = forms.IntegerField(widget=widgets.NumberInput(attrs={'hidden': True}), label='Variation')

    class Meta:
        model = InputOutputStock
        exclude = ('date', 'stock')


    def clean(self):
        if not self.cleaned_data.get('variation_hidden'):
            self.add_error('variation_hidden', "This fields is required")

        return super(InputOutputStockForm, self).clean()

    def save(self, commit=True):

        try:
            stock = Stock.objects.get(variant=self.cleaned_data.get('variation_hidden'))
            self.instance.stock = stock
        except Stock.DoesNotExist:
            stock = Stock()
            stock.variant = self.cleaned_data.get('variation_hidden')
            stock.quantity = self.cleaned_data.get('quantity')
            stock.location = 'aaa1111112' # TODO: This field is unique
            stock.save()

            iostock = InputOutputStock()
            iostock.stock = stock
            iostock.quantity = stock.quantity
            iostock.note = self.cleaned_data.get('note')

            return iostock.save()

        return super(InputOutputStockForm, self).save(commit=commit)


class StockForm(forms.ModelForm):
    class Meta:
        model = Stock
        exclude = ['reserved']

    def __init__(self, *args, **kwargs):
        product = kwargs.pop('product')
        super(StockForm, self).__init__(*args, **kwargs)
        self.fields['variant'] = forms.ModelChoiceField(
            queryset=product.variants)

from ckeditor.widgets import CKEditorWidget


class ProductForm(forms.ModelForm):
    available_on_submit = forms.DateField(widget=forms.HiddenInput(),
                                          input_formats=['%Y/%m/%d'],
                                          required=False)

    #description = forms.CharField(widget=CKEditorWidget(config_name='awesome_ckeditor'))

    class Meta:
        model = Product
        exclude = []
        widgets = {'description': CKEditorWidget(config_name='awesome_ckeditor')}


    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = pgettext_lazy(
            'Product form labels', 'Give your awesome product a name')
        self.fields['categories'].widget.attrs[
            'data-placeholder'] = pgettext_lazy('Product form labels', 'Search')
        self.fields['attributes'].widget.attrs[
            'data-placeholder'] = pgettext_lazy('Product form labels', 'Search')
        if self.instance.available_on:
            self.fields['available_on'].widget.attrs[
                'datavalue'] = self.instance.available_on.strftime('%Y/%m/%d')

    def clean(self):
        data = super(ProductForm, self).clean()
        data['available_on'] = data.get('available_on_submit')
        if data['available_on'] and 'available_on' in self._errors:
            del self._errors['available_on']
        return data


class ProductVariantForm(forms.ModelForm):
    class Meta:
        model = ProductVariant
        exclude = ['attributes', 'product']

    def __init__(self, *args, **kwargs):
        super(ProductVariantForm, self).__init__(*args, **kwargs)
        self.fields['price_override'].widget.attrs[
            'placeholder'] = self.instance.product.price.gross
        self.fields['weight_override'].widget.attrs[
            'placeholder'] = self.instance.product.weight


class VariantAttributeForm(forms.ModelForm):
    class Meta:
        model = ProductVariant
        fields = []

    def __init__(self, *args, **kwargs):
        super(VariantAttributeForm, self).__init__(*args, **kwargs)
        self.available_attrs = self.instance.product.attributes.prefetch_related(
            'values')
        for attr in self.available_attrs:
            field_defaults = {'label': attr.display,
                              'required': True,
                              'initial': self.instance.get_attribute(attr.pk)}
            if attr.has_values():
                field = forms.ModelChoiceField(queryset=attr.values.all(),
                                               **field_defaults)
            else:
                field = forms.CharField(**field_defaults)
            self.fields[attr.get_formfield_name()] = field

    def save(self, commit=True):
        attributes = {}
        for attr in self.available_attrs:
            value = self.cleaned_data.pop(attr.get_formfield_name())
            attributes[attr.pk] = value.pk if hasattr(value, 'pk') else value
        self.instance.attributes = attributes
        return super(VariantAttributeForm, self).save(commit=commit)


class VariantBulkDeleteForm(forms.Form):
    items = forms.ModelMultipleChoiceField(queryset=ProductVariant.objects)

    def delete(self):
        items = ProductVariant.objects.filter(pk__in=self.cleaned_data['items'])
        items.delete()


class StockBulkDeleteForm(forms.Form):
    items = forms.ModelMultipleChoiceField(queryset=Stock.objects)

    def delete(self):
        items = Stock.objects.filter(pk__in=self.cleaned_data['items'])
        items.delete()


class ProductImageForm(forms.ModelForm):

    class Meta:
        model = ProductImage
        exclude = ('product', 'order')

    def __init__(self, *args, **kwargs):
        super(ProductImageForm, self).__init__(*args, **kwargs)
        if self.instance.image:
            self.fields['image'].widget = ImagePreviewWidget()


class ProductAttributeForm(forms.ModelForm):
    class Meta:
        model = ProductAttribute
        exclude = []


AttributeChoiceValueFormset = inlineformset_factory(
    ProductAttribute, AttributeChoiceValue, exclude=(), extra=1)

from apps.option.models import Option, OptionSet


class OptionSetForm(forms.ModelForm):
    class Meta:
        model = OptionSet
        exclude = []
        widgets = {
            'description': forms.Textarea(),
        }

class OptionForm(forms.ModelForm):
    class Meta:
        model = Option
        exclude = ('parent', )
        widgets = {
            'description': CKEditorWidget(config_name='awesome_ckeditor')
        }

    def save(self, commit=True, *args, **kwargs):
        self.instance.parent = kwargs['parent']
        return super(OptionForm, self).save(commit=True)



OptionChoiceValueFormset = inlineformset_factory(
    OptionSet, Option, exclude=(), extra=1, fk_name='parent', can_delete=True,
        widgets={'description': CKEditorWidget(config_name='awesome_ckeditor'),
                 'DELETE': forms.BooleanField(widget=forms.CheckboxInput(attrs={'class': 'formset-delete'}))
                 })



class StockFilterForm(forms.Form):
    # product = forms.ChoiceField(choices=ORDER_STATUS_CHOICES)
    variant__product = forms.ModelChoiceField(Product.objects.all(), empty_label='All', label='Product' )