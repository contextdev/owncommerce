# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ModelGeneric',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Option',
            fields=[
                ('modelgeneric_ptr', models.OneToOneField(auto_created=True, serialize=False, to='option.ModelGeneric', parent_link=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=40)),
                ('description', models.TextField()),
                ('short_description', models.CharField(max_length=255, blank=True)),
                ('image', versatileimagefield.fields.VersatileImageField(null=True, verbose_name='image', upload_to='option', blank=True)),
            ],
            options={
                'verbose_name': 'Option',
                'verbose_name_plural': 'Options',
                'ordering': ['-title'],
            },
            bases=('option.modelgeneric',),
        ),
        migrations.CreateModel(
            name='OptionSet',
            fields=[
                ('modelgeneric_ptr', models.OneToOneField(auto_created=True, serialize=False, to='option.ModelGeneric', parent_link=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=40)),
                ('description', models.TextField()),
                ('image', models.ImageField(null=True, upload_to='option_set', blank=True)),
                ('required', models.BooleanField(default=False)),
                ('widget_type', models.CharField(choices=[('TXT', 'Text'), ('IMG', 'Image')], max_length=3, default='TXT')),
                ('category', models.ForeignKey(to='product.Category', related_name='option_sets')),
            ],
            options={
                'verbose_name': 'Option Set',
                'verbose_name_plural': 'Option Sets',
                'ordering': ['-title'],
            },
            bases=('option.modelgeneric',),
        ),
        migrations.AddField(
            model_name='option',
            name='parent',
            field=models.ForeignKey(to='option.OptionSet', related_name='options'),
        ),
    ]
