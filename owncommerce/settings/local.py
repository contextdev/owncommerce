
from .base import *

import dj_database_url

DEBUG = True


INSTALLED_APPS += [
    'django_extensions',   # for Ipython Notebook
    'debug_toolbar',
]

SQLITE_DB_URL = 'sqlite:///' + os.path.join(PROJECT_ROOT, 'dev.sqlite')

DATABASES = {'default': dj_database_url.config(default=SQLITE_DB_URL)}

#EMAIL_BACKEND="django.core.mail.backends.smtp.EmailBackend"
#DJMAIL_REAL_BACKEND="django.core.mail.backends.smtp.EmailBackend"


SITE_URL = "http://www.joanshop.com.ar/"

DEFAULT_FROM_EMAIL = 'testnubiquo@gmail.com'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = DEFAULT_FROM_EMAIL
EMAIL_HOST_PASSWORD = 'nubiquo1234567890'
EMAIL_USE_TLS = True


TIME_ZONE = 'America/Argentina/Cordoba'
LANGUAGE_CODE = 'es'


FACEBOOK_APP_ID = '527891700709765'
FACEBOOK_SECRET = 'a63ca2cc560bf46770183b39368b4b68'


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
            '%(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'filters': ['require_debug_true'],
            'formatter': 'simple'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True
        },
        'saleor': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True
        }
    }
}

SITE_DEFAULT_DATA = {
    "logo": "/static/image/logo_context.png",
    "name": "TiendaNet",
    "display_name": "TiendaNet",
    "site_url": "http://localhost",
    "slogan": "Una Tienda Online para Negocios como el tuyo",
    "contact_email": "contacto@context.com.ar",
    "skype": "",
    "phones": ["+54 9 351 6222930",],
    "emails": ["contacto@context.com.ar",]
}