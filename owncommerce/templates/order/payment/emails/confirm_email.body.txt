{% load i18n %}

{% block content %}{% blocktrans %}Hi!

Thank you for your payment. To see your payment details please visit:
{{ order_url }}

Sincerely,
SITE_NAME {% endblocktrans %}{% endblock %}
