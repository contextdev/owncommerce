


INSTALLED_APPS = [
    # External apps that need to go before django's
    #'offsite_storage',
    'cmsplugin_cascade',
    'cmsplugin_cascade.extra_fields',  # optional
    'cmsplugin_cascade.sharable',  # optional
    'cms',
    'menus',
    'sekizai',
    'treebeard',
    'djangocms_admin_style',
    'djangocms_text_ckeditor',

    # Django modules
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.webdesign',

    # CMS apps
    'djangocms_style',
    #'djangocms_column', This replace by bootstrap (cascade)
    'djangocms_file',
    #'djangocms_flash',
    'djangocms_googlemap',
    'djangocms_inherit',
    #'djangocms_link', This replace by bootstrap (cascade)
    #'djangocms_picture', This replace by bootstrap (cascade)
    'djangocms_teaser',
    'djangocms_video',
    'reversion',

    'owncommerce', # TODO: Esto va?

    # Ecommerce apps
    'saleor.userprofile',
    'saleor.product',
    'saleor.cart',
    'saleor.checkout',
    'saleor.core',
    'saleor.order',
    'saleor.registration',
    'saleor.dashboard',

    # Third apps
    'versatileimagefield',
    'babeldjango',
    'django_prices',
    'emailit',
    'mptt',
    'payments',
    'selectable', # TODO: This packages not used
    'materializecssform',
    'rest_framework',
    'jsonify',
    'bootstrap3',
    'favit',
    'spurl',
    'djmail',
    'ckeditor',
    'filer',
    'easy_thumbnails',
    'django_select2',
    'sorl.thumbnail',

    # My apps
    'apps.option',
    'apps.common_tags',
    'apps.rating',
    'apps.products',
    'apps.custom_theme',
    'apps.custom_site',


    # My plugins
    'apps.pluggin_featured_product',
    'apps.pluggin_option',
    'apps.pluggin_button_social',


    # Third plugins cms
    'cmsplugin_contact_plus',
    'djangocms_forms',
    #'cmsplugin_filer_image', This replace by image bootstrap (cascade)

    'djangocms_fbcomments',
    'django_filters',

    'redactor',
    'django_admin_dialog',
]