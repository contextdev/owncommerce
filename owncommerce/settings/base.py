import ast
import os.path


from django.contrib.messages import constants as messages

gettext = lambda s: s
DATA_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = ast.literal_eval(os.environ.get('DEBUG', 'True'))

SITE_ID = 1

PROJECT_ROOT = os.path.normpath(os.path.join(os.path.join(os.path.dirname(__file__), '../..')))


ROOT_URLCONF = 'owncommerce.urls'

WSGI_APPLICATION = 'saleor.wsgi.application'



INTERNAL_IPS = os.environ.get('INTERNAL_IPS', '127.0.0.1').split()

#
# Configure Time zone and language
# ========================================================
#

USE_I18N = True
USE_L10N = True
USE_TZ = True

LANGUAGES = (
    ('es', gettext('es')),
    #('en', gettext('en')),
)

#
# Configure Static and Media Files
# ========================================================
#

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')
MEDIA_URL = '/media/'

STATIC_ROOT =  os.path.join(os.path.dirname(PROJECT_ROOT), "static")

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, 'static'),
    os.path.join(PROJECT_ROOT, 'saleor', 'static'),
)

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder'
]


#
# Configure Templates
# ========================================================
#

context_processors = [
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.i18n',
    'django.core.context_processors.debug',
    'django.core.context_processors.request',
    #'django.core.context_processors.media',
    'django.core.context_processors.csrf',
    'django.core.context_processors.tz',
    'sekizai.context_processors.sekizai',
    'django.core.context_processors.static',

    'saleor.core.context_processors.canonical_hostname',
    'saleor.core.context_processors.default_currency',
    'saleor.core.context_processors.categories',

    'apps.custom_site.context_processors.custom_site_preprocessor',

    'cms.context_processors.cms_settings',
    'django_admin_dialog.context_processors.django_admin_dialog',
]

loaders = [
    #'apps.custom_theme.template_loader.Loader',
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # TODO: this one is slow, but for now need for mptt?
    'django.template.loaders.eggs.Loader']


TEMPLATES = [{
                 'BACKEND': 'django.template.backends.django.DjangoTemplates',
                 'DIRS': [
                     os.path.join(PROJECT_ROOT, 'templates'),
                     os.path.join(PROJECT_ROOT, 'owncommerce', 'templates')
                 ],
                 'OPTIONS': {
                     'debug': DEBUG,
                     'context_processors': context_processors,
                     'loaders': loaders
                     #'string_if_invalid': '<< MISSING VARIABLE >>'
                 }}]


# Make this unique, and don't share it with anybody.
SECRET_KEY = os.environ.get('SECRET_KEY', 'd0au3t()-t0&^u%z*jp+kosz@9iq#r_(4n6turx$&pcwhj7eq1')



MIDDLEWARE_CLASSES = [

    'django.contrib.sessions.middleware.SessionMiddleware',
    'apps.custom_site.middleware.CustomSiteMiddleware', # TODO: Comment when production
    'apps.custom_theme.middleware.CheckConfigThemeMiddleware', # TODO: Comment when production
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',

    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',

    'saleor.cart.middleware.CartMiddleware',
    'saleor.core.middleware.DiscountMiddleware',
    'saleor.core.middleware.GoogleAnalytics',
    'saleor.core.middleware.CheckHTML',

    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',

    'babeldjango.middleware.LocaleMiddleware',
    ]

from .installed_app import INSTALLED_APPS

INSTALLED_APPS = INSTALLED_APPS


AUTHENTICATION_BACKENDS = (
    'saleor.registration.backends.EmailPasswordBackend',
    'saleor.registration.backends.ExternalLoginBackend',
    'saleor.registration.backends.TrivialBackend'
)

AUTH_USER_MODEL = 'userprofile.User'

CANONICAL_HOSTNAME = os.environ.get('CANONICAL_HOSTNAME', 'localhost:8000')

LOGIN_URL = '/account/login'

WARN_ABOUT_INVALID_HTML5_OUTPUT = False

DEFAULT_CURRENCY = 'ARS'
DEFAULT_WEIGHT = 'lb'

ACCOUNT_ACTIVATION_DAYS = 3

LOGIN_REDIRECT_URL = '/'

FACEBOOK_APP_ID = os.environ.get('FACEBOOK_APP_ID')
FACEBOOK_SECRET = os.environ.get('FACEBOOK_SECRET')

GOOGLE_ANALYTICS_TRACKING_ID = os.environ.get('GOOGLE_ANALYTICS_TRACKING_ID')
GOOGLE_CLIENT_ID = os.environ.get('GOOGLE_CLIENT_ID')
GOOGLE_CLIENT_SECRET = os.environ.get('GOOGLE_CLIENT_SECRET')

PAYMENT_BASE_URL = 'http://%s/' % CANONICAL_HOSTNAME

PAYMENT_MODEL = 'order.Payment'

PAYMENT_VARIANTS = {
    'default': ('payments.dummy.DummyProvider', {}),
    'mercadopago': ('apps.payment_methods.MercadoPagoProvider', {
        'client_id': "8469633778057998", # TODO: This data is to user test
        'client_secret': "li2Y5FzdDSzgRhPPZfhxtN3gNYqGsNTn", # TODO: This data is to user test
        'sandbox': True
    }),
    'paypal': ('payments.paypal.PaypalProvider', {
        'client_id': 'ASnASvtYeOLoeD-LR93nEGC9ySlA7PpCHc868Lf8Ijg4ezOQJGKVzw4GTqIAgDQ2APq826ozD3er5DkV',
        'secret': 'EJzo3Tvi-zLTY19CXFkRhItRiiws2vLb_d38VMDcLu3sJquJv63ddpkrudeU7k4Y7qmamGfAJYZl_ovi',
        'endpoint': 'https://api.sandbox.paypal.com',
        'capture': True})
}

# MERcadoPago Datos reales
# Client id  7681839563434648
# Client secret  dXnXWdFfH386icIzWIjIcUpmSXLdLQFx

PAYMENT_HOST = os.environ.get('PAYMENT_HOST', 'localhost:8000')

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'

CHECKOUT_PAYMENT_CHOICES = [
    ('default', 'Dummy provider'),
    ('mercadopago', "MercadoPago"),
    ('paypal', 'PayPal')
]

MESSAGE_TAGS = {
    messages.ERROR: 'danger',
    }

LOW_STOCK_THRESHOLD = 10


ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', 'localhost').split()

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Amazon S3 configuration
# AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
# AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
# AWS_STATIC_BUCKET_NAME = os.environ.get('AWS_STATIC_BUCKET_NAME')
#
# AWS_MEDIA_ACCESS_KEY_ID = os.environ.get('AWS_MEDIA_ACCESS_KEY_ID')
# AWS_MEDIA_SECRET_ACCESS_KEY = os.environ.get('AWS_MEDIA_SECRET_ACCESS_KEY')
# AWS_MEDIA_BUCKET_NAME = os.environ.get('AWS_MEDIA_BUCKET_NAME')
#
# if AWS_STATIC_BUCKET_NAME:
#     STATICFILES_STORAGE = 'offsite_storage.storages.CachedS3FilesStorage'
#
# if AWS_MEDIA_BUCKET_NAME:
#     DEFAULT_FILE_STORAGE = 'offsite_storage.storages.S3MediaStorage'
#     THUMBNAIL_DEFAULT_STORAGE = DEFAULT_FILE_STORAGE

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'


# EMAIL_BACKEND = ('django.core.mail.backends.%s.EmailBackend' %
#                  os.environ.get('EMAIL_BACKEND_MODULE', 'console'))

EMAIL_BACKEND="djmail.backends.async.EmailBackend"
DJMAIL_REAL_BACKEND="django.core.mail.backends.smtp.EmailBackend"


EMAIL_HOST = os.environ.get('EMAIL_HOST')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
EMAIL_PORT = os.environ.get('EMAIL_PORT')
EMAIL_USE_TLS = ast.literal_eval(os.environ.get('EMAIL_USE_TLS', 'False'))
DEFAULT_FROM_EMAIL = os.environ.get('DEFAULT_FROM_EMAIL')



# Configure location to search translate
LOCALE_PATHS = (
    os.path.join(PROJECT_ROOT, 'locale'),
)



#
# Configure Django CMS
# ========================================================
#

CMS_LANGUAGES = {
    ## Customize this
    'default': {
        'public': True,
        'hide_untranslated': False,
        'redirect_on_fallback': True,
        },
    1: [
        {
            'code': 'es',
            'public': True,
            'hide_untranslated': False,
            'name': gettext('es'),
            'redirect_on_fallback': True,
            },
        # {
        #     'code': 'en',
        #     'public': True,
        #     'hide_untranslated': False,
        #     'name': gettext('en'),
        #     'redirect_on_fallback': True,
        #     },
    ],
    }

CMS_TEMPLATES = (
    ## Customize this
    ('page.html', 'Page'),
    ('feature.html', 'Page with Feature'),
    ('page_sidebar_left.html', 'Page Sidebar Left'),
    ('page_sidebar_right.html', 'Page Sidebar Right'),
    ('placeholders_product_app.html', 'Page for App')
)

CMS_PERMISSION = True

CMS_PLACEHOLDER_CONF = {
    'Page Content': {
        'plugins': ['BootstrapContainerPlugin'],
        },
    'Footer-social': {
        'plugins': ['CMSButtonSocialtPlugin'],
        },
    # 'Content':{
    #     'plugins': ['BootstrapContainerPlugin', 'CMSFeaturedProductPlugin']
    # }
}


#
# Module migration
# ========================================================
#
MIGRATION_MODULES = {
    'cms': 'cms.migrations',
    'djangocms_googlemap': 'djangocms_googlemap.migrations_django',
    'djangocms_teaser': 'djangocms_teaser.migrations_django',
    'djangocms_file': 'djangocms_file.migrations_django',
    'djangocms_picture': 'djangocms_picture.migrations_django',
    'djangocms_link': 'djangocms_link.migrations_django',
    'djangocms_flash': 'djangocms_flash.migrations_django',
    'djangocms_column': 'djangocms_column.migrations_django',
    'djangocms_inherit': 'djangocms_inherit.migrations_django',
    'djangocms_style': 'djangocms_style.migrations_django',
    'djangocms_video': 'djangocms_video.migrations_django',
    #'djangocms_owl': 'djangocms_owl.south_migrations',
    'cmsplugin_filer_file': 'cmsplugin_filer_file.migrations_django',
    'djangocms_text_ckeditor': 'djangocms_text_ckeditor.migrations_django',
    'cmsplugin_filer_image': 'cmsplugin_filer_image.migrations_django',
    }


#
# Plugins Own Carousel (DJANGO CMS)
# ========================================================
#

# DJANGOCMS_OWL_STYLES = (
#     ('style1', 'Style 1'),
#     ('style2', 'Style 2'),
# )
#
# DJANGOCMS_OWL_TEMPLATES = (
#     ('template1', 'Template 1'),
#     ('template2', 'Template 2'),
# )
#
# DJANGOCMS_OWL_CHILD_CLASSES = (
#     'PicturePlugin',
# )
#
# DJANGOCMS_OWL_INCLUDE_CSS = True
# DJANGOCMS_OWL_INCLUDE_JS_OWL = True
# DJANGOCMS_OWL_INCLUDE_JS_JQUERY = True


#
# Plugins Own Carousel (DJANGO CMS)
# ========================================================
#
DJANGOCMS_FORMS_RECAPTCHA_PUBLIC_KEY = '6LcqzgwTAAAAADsuGYngNnerWj4K4WwfxBELIrA7'
DJANGOCMS_FORMS_RECAPTCHA_SECRET_KEY = '6LcqzgwTAAAAAORR7QVOMitQM0nczaN1Yh84pGIq'

DJANGOCMS_FORMS_WIDGET_CSS_CLASSES = {'__all__': ('form-control', ) }

#
# Plugin FILER
# ==========================================================
#

THUMBNAIL_HIGH_RESOLUTION = True

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

#
# CKEditor widget
# ==========================================================
#
CKEDITOR_UPLOAD_PATH = "uploads/"
# TODO: Import jquery by template and quiet this line
CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'

# Defined custom functionality to ckeditor
CKEDITOR_CONFIGS = {
    'awesome_ckeditor': {
        'toolbar': [
            ["Format", "Bold", "Italic", "Underline", "Strike", "SpellChecker", 'TextColor'],
            ['NumberedList', 'BulletedList', "Indent", "Outdent", 'JustifyLeft', 'JustifyCenter',
             'JustifyRight', 'JustifyBlock'],
            ["Table", "Link", "Unlink", "SectionLink", "Subscript", "Superscript"], ['Undo', 'Redo'],
            ["Maximize"]
        ],
        "removePlugins": "stylesheetparser",
        'uiColor' : '#f5f5f6',
        'with': '100%'
    },
    }


#
# Config to Disqus
# ==========================================
#
CONNECTED_ACCOUNTS_DISQUS_CONSUMER_KEY = 'd4Mt3oOqysexIG1gub0iXrQGcJr0BosjFoLUs9VtuMoeXqy0kIuMby42mRRyAWdg'
CONNECTED_ACCOUNTS_DISQUS_CONSUMER_SECRET = 'QYzytKA0whje4rVVkpgTuYmcC2cr5xmKLYhzPY32gSy0GWY1FB2edaPNghW94zph'

#
# Config cms cascade
# ==========================================
#
#CMS_CASCADE_PLUGINS = ('bootstrap3',)

CMSPLUGIN_CASCADE_PLUGINS = ('cmsplugin_cascade.bootstrap3',)

# CMS_CASCADE_LEAF_PLUGINS = (
#     'TextPlugin',
#     'PicturePlugin',
#     'LinkPlugin',
#     'StylePlugin',
#     'FormBuilderPlugin',
#     'FilePlugin',
#     #'VideoPlugin',
#     'TabHeaderPlugin',
#     'AccordionHeaderPlugin',
#     'RepositoryDashboardPlugin',
#     'SessionDashboardPlugin',
#     'CMSLatestNewsPlugin',
# )
#
CMSPLUGIN_CASCADE_WITH_EXTRAFIELDS = (
    'BootstrapButtonPlugin',
    'BootstrapRowPlugin',
    'SimpleWrapperPlugin',
    'HorizontalRulePlugin',
)

CMSPLUGIN_CASCADE_ALIEN_PLUGINS = (
    'TextPlugin',
    'FilerImagePlugin',
    'VideoPlugin',
    'CMSFeaturedProductPlugin',
    'FacebookCommentsPlugin',
)

THEMES = [
    {
        "name": "Venedor",
        "config": {
            "color": {
                "default": "blue",
                "widget": "option",
                "options": ["blue" , "blueorange" , "blueorange2", "brown" , "browngreen" , "green" ,"green2" ,"orange" ,"pink"]
            },
            "header": {
                "default": "_header1.html",
                "widget": "option",
                "options": ["_header1.html" , "_header2.html" , "_header3.html", "_header4.html" , "_header5.html" , "_header6.html" ,"_header7.html"]
            },
            "content": {
                "default": "fullwidth",
                "widget": "option",
                "options": ["boxed" , "fullwidth"]
            }
        }
    }
]

AUTOMATICALLY_ACCEPT_ORDER = False
DJANGO_ADMIN_DIALOG_SHOW_IDS = True