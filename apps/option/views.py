from django.shortcuts import render

from django.views.generic import DetailView
from .models import Option


class OptionDetailView(DetailView):
    model = Option
    template_name = 'option/detail.html'
    queryset = Option.objects.all()



