# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import django.db.models.deletion
from django.conf import settings
import django_prices.models
import satchless.item
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('product', '0001_initial'),
        ('userprofile', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeliveryGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('status', models.CharField(choices=[('new', 'Processing'), ('cancelled', 'Cancelled'), ('shipped', 'Shipped')], verbose_name='delivery status', max_length=32, default='new')),
                ('shipping_required', models.BooleanField(verbose_name='shipping required', default=True)),
                ('shipping_price', django_prices.models.PriceField(verbose_name='shipping price', max_digits=12, decimal_places=4, editable=False, currency='$', default=0)),
            ],
            bases=(models.Model, satchless.item.ItemSet),
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('status', models.CharField(choices=[('new', 'Pending'), ('processing', 'processing'), ('cancelled', 'Cancelled'), ('payment-pending', 'Waiting for payment'), ('fully-paid', 'Fully paid'), ('shipped', 'Shipped')], verbose_name='order status', max_length=32, default='new')),
                ('created', models.DateTimeField(editable=False, verbose_name='created', default=django.utils.timezone.now)),
                ('last_status_change', models.DateTimeField(editable=False, verbose_name='last status change', default=django.utils.timezone.now)),
                ('tracking_client_id', models.CharField(editable=False, max_length=36, blank=True)),
                ('shipping_method', models.CharField(verbose_name='Delivery method', max_length=255, blank=True)),
                ('anonymous_user_email', models.EmailField(editable=False, max_length=254, blank=True, default='')),
                ('token', models.CharField(unique=True, verbose_name='token', max_length=36)),
                ('billing_address', models.ForeignKey(to='userprofile.Address', editable=False, related_name='+')),
                ('shipping_address', models.ForeignKey(null=True, to='userprofile.Address', editable=False, related_name='+')),
                ('user', models.ForeignKey(null=True, verbose_name='user', to=settings.AUTH_USER_MODEL, blank=True, related_name='orders')),
            ],
            options={
                'ordering': ('-last_status_change',),
            },
            bases=(models.Model, satchless.item.ItemSet),
        ),
        migrations.CreateModel(
            name='OrderedItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('product_name', models.CharField(verbose_name='product name', max_length=128)),
                ('product_sku', models.CharField(verbose_name='sku', max_length=32)),
                ('quantity', models.IntegerField(verbose_name='quantity', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(999)])),
                ('unit_price_net', models.DecimalField(verbose_name='unit price (net)', max_digits=12, decimal_places=4)),
                ('unit_price_gross', models.DecimalField(verbose_name='unit price (gross)', max_digits=12, decimal_places=4)),
                ('extra_data', models.CharField(null=True, max_length=200, blank=True, default='default')),
                ('delivery_group', models.ForeignKey(to='order.DeliveryGroup', editable=False, related_name='items')),
                ('product', models.ForeignKey(null=True, verbose_name='product', to='product.Product', on_delete=django.db.models.deletion.SET_NULL, blank=True, related_name='+')),
            ],
            bases=(models.Model, satchless.item.ItemLine),
        ),
        migrations.CreateModel(
            name='OrderHistoryEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('date', models.DateTimeField(editable=False, verbose_name='last history change', default=django.utils.timezone.now)),
                ('status', models.CharField(choices=[('new', 'Pending'), ('processing', 'processing'), ('cancelled', 'Cancelled'), ('payment-pending', 'Waiting for payment'), ('fully-paid', 'Fully paid'), ('shipped', 'Shipped')], verbose_name='order status', max_length=32)),
                ('comment', models.CharField(max_length=100, blank=True, default='')),
                ('order', models.ForeignKey(to='order.Order', related_name='history')),
                ('user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, blank=True)),
            ],
            options={
                'ordering': ['date'],
            },
        ),
        migrations.CreateModel(
            name='OrderNote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('content', models.CharField(max_length=250)),
                ('order', models.ForeignKey(to='order.Order', related_name='notes')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('variant', models.CharField(max_length=255)),
                ('status', models.CharField(choices=[('waiting', 'Waiting for confirmation'), ('preauth', 'Pre-authorized'), ('confirmed', 'Confirmed'), ('rejected', 'Rejected'), ('refunded', 'Refunded'), ('error', 'Error'), ('input', 'Input')], max_length=10, default='waiting')),
                ('fraud_status', models.CharField(choices=[('unknown', 'Unknown'), ('accept', 'Passed'), ('reject', 'Rejected'), ('review', 'Review')], verbose_name='fraud check', max_length=10, default='unknown')),
                ('fraud_message', models.TextField(blank=True, default='')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('transaction_id', models.CharField(max_length=255, blank=True)),
                ('currency', models.CharField(max_length=10)),
                ('total', models.DecimalField(max_digits=9, decimal_places=2, default='0.0')),
                ('delivery', models.DecimalField(max_digits=9, decimal_places=2, default='0.0')),
                ('tax', models.DecimalField(max_digits=9, decimal_places=2, default='0.0')),
                ('description', models.TextField(blank=True, default='')),
                ('billing_first_name', models.CharField(max_length=256, blank=True)),
                ('billing_last_name', models.CharField(max_length=256, blank=True)),
                ('billing_address_1', models.CharField(max_length=256, blank=True)),
                ('billing_address_2', models.CharField(max_length=256, blank=True)),
                ('billing_city', models.CharField(max_length=256, blank=True)),
                ('billing_postcode', models.CharField(max_length=256, blank=True)),
                ('billing_country_code', models.CharField(max_length=2, blank=True)),
                ('billing_country_area', models.CharField(max_length=256, blank=True)),
                ('billing_email', models.EmailField(max_length=254, blank=True)),
                ('customer_ip_address', models.IPAddressField(blank=True)),
                ('extra_data', models.TextField(blank=True, default='')),
                ('message', models.TextField(blank=True, default='')),
                ('token', models.CharField(max_length=36, blank=True, default='')),
                ('captured_amount', models.DecimalField(max_digits=9, decimal_places=2, default='0.0')),
                ('order', models.ForeignKey(to='order.Order', related_name='payments')),
            ],
            options={
                'ordering': ('-pk',),
            },
        ),
        migrations.AddField(
            model_name='deliverygroup',
            name='order',
            field=models.ForeignKey(to='order.Order', editable=False, related_name='groups'),
        ),
    ]
