import django.dispatch

post_order_change_status = django.dispatch.Signal(providing_args=["old_status", "new_status"])

post_delivery_group_change_status = django.dispatch.Signal(providing_args=["old_status", "new_status"])