from django.contrib import admin
from .models import CustomCss, CustomCssAttribute, Theme
from django.forms import ModelForm, Textarea, TextInput, MultipleChoiceField, Select, ChoiceField

WIDGETS_TYPE = {
    'color': 'color',
    'background-color': 'color',
    'font': 'color',
    'font-size': 'color',
    # 'width': 'number'
}

ATTRIBUTE_CHOICES = (
    ('', 'Choose'),
    ('font-family', 'font-family'),
    ('color', 'color'),
    ('background-color', 'background-color'),
    ('width', 'width'),
    ('margin', 'margin'),
)

FONT_CHOICES = (
    ('', 'Choose'),
    ("Tangerine, Open Sans, sans-serif", 'font-1'),
    ("Oswald, sans-serif", 'font-2'),
    ("Roboto Condensed, sans-serif", 'font-3'),
)


class CssValueTextInputWidget(TextInput):
    class Media:
        css = {
            'all': ('custom_css/css/attr.css',)
        }
        js = ('custom_css/js/attr_value_widget.js',)


class AttrAdminForm(ModelForm):

    attr_name = ChoiceField(required=False, widget=Select, choices=ATTRIBUTE_CHOICES)

    class Meta:
        model = CustomCssAttribute
        exclude = []
        widgets = {'value': CssValueTextInputWidget()}

    def __init__(self, *args, **kwargs):
        super(AttrAdminForm, self).__init__(*args, **kwargs)
        try:
            if self.initial['attr_name'] == 'font-family':
                self.fields['value'].widget = Select(choices=FONT_CHOICES)
            else:
                self.fields['value'].widget.input_type = WIDGETS_TYPE.get(self.initial['attr_name'], 'text')
            self.fields['value'].widget.attrs['data-attr-name'] = self.initial['attr_name']
            self.fields['value'].widget.attrs['data-selector'] = self.instance.selector.selector
        except:
            pass


class CustomCssAttributeAdmin(admin.ModelAdmin):
    pass


class CustomCssAttributeInline(admin.TabularInline):
    model = CustomCssAttribute
    form = AttrAdminForm


class CustomCssAdmin(admin.ModelAdmin):
    list_display = ('display_name', 'selector')
    inlines = [CustomCssAttributeInline,]

    def get_queryset(self, request):
        return CustomCss.objects.all()

class ThemeAdmin(admin.ModelAdmin):
    list_display = ('name', 'active', 'path')

admin.site.register(CustomCss, CustomCssAdmin)
admin.site.register(CustomCssAttribute, CustomCssAttributeAdmin)
admin.site.register(Theme, ThemeAdmin)