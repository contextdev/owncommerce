from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class CustomSite(models.Model):
    name = models.CharField(max_length=60)
    display_name = models.CharField(max_length=60)
    site_url = models.CharField(max_length=60)
    slogan = models.CharField(max_length=60, blank=True)
    contact_email = models.CharField(max_length=60, blank=True)
    skype = models.CharField(max_length=60, blank=True)
    logo = models.ImageField(upload_to='custom-site', null=True, blank=True, )

    def __str__(self):
        return self.display_name


class CustomSiteEmail(models.Model):
    custom_site = models.ForeignKey(CustomSite, related_name='emails')
    email = models.EmailField()

    def __str__(self):
        return self.email


class CustomSitePhone(models.Model):
    contact = models.ForeignKey(CustomSite, related_name='phones')
    phone_number = PhoneNumberField()

    def __str__(self):
        return self.phone_number.__str__()


class CustomSiteEmailAccount(models.Model):
    custom_site = models.ForeignKey(CustomSite, unique=True)
    company = models.CharField(max_length=20)
    email = models.EmailField()
    email_pass = models.CharField(max_length=60)
    email_host = models.CharField(max_length=20)
    email_port = models.IntegerField(help_text='GMAIL use to be 587')
    use_tls = models.BooleanField(default=True)

    def __str__(self):
        return self.email
