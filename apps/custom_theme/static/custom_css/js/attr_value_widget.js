var custom_css_sheet;
var rules = {};
(function($) {
    $( document ).ready(function() {

        for(i=0; i<parent.document.styleSheets.length;i++) {
            if(parent.document.styleSheets[i].ownerNode.id == "custom-css") {
                custom_css_sheet = parent.document.styleSheets[i];

                for(j=0; j<custom_css_sheet.cssRules.length; j++) {
                    rules[custom_css_sheet.cssRules[j].selectorText] = custom_css_sheet.cssRules[j];
                }
            }
        }

        $('[name$=-value]').on('input',function(e){
            value =  $(this).val();
            var attr_name = $(this)[0].getAttribute('data-attr-name');
            var selector = $(this)[0].getAttribute('data-selector');
            rules[selector].style[attr_name] = value;
        });
    });
})(django.jQuery);
