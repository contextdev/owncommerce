
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _
from .models import ButtonSocialPlugin


class CMSButtonSocialtPlugin(CMSPluginBase):

    model = ButtonSocialPlugin  # model where plugin data are saved
    module = _("ButtonSocials")
    name = _("Button Social")  # name of the plugin in the interface
    render_template = "button_social/button_social.html"

    def render(self, context, instance, placeholder):
        context.update({'button': instance})
        return context


plugin_pool.register_plugin(CMSButtonSocialtPlugin)  # register the plugin
