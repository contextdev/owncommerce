#!/usr/bin/python3
import os
from datetime import date
from owncommerce.settings.installed_app import INSTALLED_APPS


exceptions = {}


settings = 'owncommerce.settings.local'

for app in INSTALLED_APPS:
    print("DUMP TO %s \n" %(app) + 20*"=")


    if len(str(app).split('.')) > 1:
        app_name = str(app).split('.')[len(str(app).split('.')) -1]
    else:
        app_name = app

    try:
        os.system("python manage.py dumpdata --indent 2 %s > dumps/%s_%s.json --settings=%s" %(app_name, app_name, date.today(), settings))
        print("SUCCESS DUMP TO %s \n" %(app) + 20*"=" )
    except Exception as e:
        exceptions[app] = e
        print("ERROR DUMP TO %s \n" %(app) + 20*"=")


if not exceptions:
    print(40*"=" + "\n The dump success!!! \n" + 40*"=")
else:
    print(40*"=" + "\n The dump error!!! \n" + 40*"=")
    print(exceptions)