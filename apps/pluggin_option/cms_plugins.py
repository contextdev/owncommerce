from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import OptionPlugin
from django.utils.translation import ugettext as _


class CMSOptionPlugin(CMSPluginBase):
    """
        Plugin to show list options to option set
    """
    model = OptionPlugin  # model where plugin data are saved
    module = _("Options")
    name = _("Option Plugin")  # name of the plugin in the interface
    render_template = "option/index.html"

    def render(self, context, instance, placeholder):
        return super(CMSOptionPlugin, self).render(context, instance, placeholder)


plugin_pool.register_plugin(CMSOptionPlugin)  # register the plugin
