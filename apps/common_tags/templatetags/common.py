from django import template
from django.utils.safestring import mark_safe
import json
from saleor.product.models import AttributeChoiceValue, ProductAttribute, Product
from django.db.models import Q


register = template.Library()

@register.filter(name='string_option')
def string_option(values):
    extra_data = ""
    from apps.option.models import Option
    import ast

    for option_set, option in ast.literal_eval(values):
       opt = Option.objects.get(id=option)
       extra_data += "<strong> %s :</strong> %s ...<br/>" %(opt.parent.title, opt.title)

    return extra_data


@register.filter(name='addcss')
def addcss(field, css):
   return field.as_widget(attrs={"class":css})

@register.filter
def product_image_url(pk):
    # TODO: performance?
    try:
        return AttributeChoiceValue.objects.get(id=pk).images.first().image.url
    except:
        return '/static/image/product-default.png'



@register.filter(name='json')
def json_dumps(data):
    return mark_safe(json.dumps(data))

@register.filter(name='product_attr_name')
def product_attr_name(arg1, arg2):
    try:
        return ProductAttribute.objects.get(id=arg2).name
    except:
        return ''

@register.filter(name='product_attr_display')
def product_attr_display(arg1, arg2):
    try:
        return AttributeChoiceValue.objects.get(id=arg1[arg2]).display
    except:
        return ''

@register.filter(name='product_attr_color')
def product_attr_color(arg1, arg2):
    try:
        return AttributeChoiceValue.objects.get(id=arg1[arg2]).color
    except:
        return ''

# @register.tag(name="related_category_products")
@register.assignment_tag(name="related_category_products")
def related_category_products(product_id, category_id, quantity=5):
    # return "asldfjklk"
    return Product.objects.filter(~Q(id = 3))[:quantity]


import ipdb

@register.filter('debug')
def debug(obj):
    ipdb.set_trace()
    return obj

