from django.db import models


class CustomCss(models.Model):
    selector = models.CharField(blank=False, max_length=60) #h1
    display_name = models.CharField(blank=False, max_length=60)

    def __str__(self):
        return self.display_name


class CustomCssAttribute(models.Model):
    selector = models.ForeignKey(CustomCss, related_name='attributes')
    attr_name = models.CharField(max_length=60) # size, bg, border
    value = models.CharField(max_length=120, blank=True, null=True) # # 20px

    def __str__(self):
        return self.selector.display_name + ' ' + self.attr_name


class Theme(models.Model):
    name = models.CharField(max_length=20)
    path = models.CharField(max_length=60)
    active = models.BooleanField()

    def save(self, *args, **kwargs):
        if self.active:
            qs = Theme.objects.filter(active=True)
            if self.pk:
                qs = qs.exclude(pk=self.pk)
            if qs.count() != 0:
                qs.update(active=False) # make this obj "the chosen one"
        super(Theme, self).save(*args, **kwargs)