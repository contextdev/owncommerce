from django.conf.urls import patterns, url, include

from rest_framework.routers import DefaultRouter

from apps.products.views import ProductViewSet, ProductVariantSet


router = DefaultRouter()
router.register(r'products', ProductViewSet, base_name='products')
router.register(r'variants', ProductVariantSet, base_name='variants')


urlpatterns = patterns('',
                        # Include router api
                        url(r'^', include(router.urls)),
                         )
