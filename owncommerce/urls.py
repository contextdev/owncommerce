from __future__ import print_function

from saleor.urls import urlpatterns as saleor_urls
from apps.products.urls import urlpatterns as products_url

from apps.favorite.views import UserFavoritesListView

from cms.sitemaps import CMSSitemap
from django.conf.urls import *  # NOQA
from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

# change "patterns" to "i18n_patterns" for add suppor with multiple language

urlpatterns = patterns('',
                            url(r'^admin/', include(admin.site.urls)),  # NOQA
                            url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
                                {'sitemaps': {'cmspages': CMSSitemap}}),
                            url(r'^select2/', include('django_select2.urls')),
                            url(r'^selectable/', include('selectable.urls')), # TODO: This packages not used
                            url(r'^user-favorites/$', UserFavoritesListView.as_view(), name='user-favorites'),
                            url(r'^favit/', include('favit.urls')),
                            url(r'^option/', include('apps.option.urls', namespace='option')),
                            url(r'^calification/', include('apps.rating.urls', namespace="rating")),
                            url(r'^theme-config/', include('apps.custom_theme.urls', namespace="custom_theme")),
                            (r'^ckeditor/', include('ckeditor_uploader.urls')),
                            url(r'^', include(saleor_urls), name='saleor'),
                            url(r'^', include('djangocms_forms.urls')),
                            url(r'^', include('cms.urls')),
                            )

urlpatterns = patterns('',
                       (r'^api/v1/', include('owncommerce.urls_api', namespace='api')),
                        ) + urlpatterns

# This is only needed when using runserver.
#if settings.DEBUG:
urlpatterns = patterns('',
                       url(r'^media/(?P<path>.*)$', 'django.views.static.serve',  # NOQA
                           {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
                       ) + staticfiles_urlpatterns() + urlpatterns  # NOQA