from django.conf.urls import patterns, url

from .views import OptionDetailView

urlpatterns = patterns('',
                       url(r"^(?P<pk>[0-9]+)/$", OptionDetailView.as_view(), name="detail"),
                        )
