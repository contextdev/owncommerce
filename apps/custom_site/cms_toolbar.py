from django.utils.translation import ugettext_lazy as _
from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar
from cms.utils.urlutils import admin_reverse
from .models import CustomSite


@toolbar_pool.register
class CustomSiteToolbar(CMSToolbar):
    watch_models = [CustomSite, ]

    def populate(self):
        menu = self.toolbar.get_or_create_menu('custom-site', _('Site'))

        custom_site = CustomSite.objects.all().first()

        if custom_site:
            url = admin_reverse('custom_site_customsite_change', args=(custom_site.pk,))
            name=_('Edit Site Data')
        else:
            url=admin_reverse('custom_site_customsite_add')
            name=_('Set Site Data')

        menu.add_modal_item(name= name, url=url, )


        #
        # menu.add_sideframe_item(
        #     name=_('Custom Site Contact Data list'),
        #     url=admin_reverse('custom_site_customsitecontactdata_changelist'),
        #     # url=admin_reverse('custom_css_customcss_changelist'),
        # )
