from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe

from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from cms.menu_bases import CMSAttachMenu


from saleor.product.models import Product, Category

class ProductSubMenu(CMSAttachMenu):
    name = _("Product sub-menu")

    def get_nodes(self, request):
        nodes = []
        for category in Category.objects.order_by('name').all():
            node = NavigationNode(
                    mark_safe(category.name),
                    category.get_absolute_url(),
                    category.id,
                    )
            nodes.append(node)

        return nodes

menu_pool.register_menu(ProductSubMenu)
