from .prod import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'owncommerce',
        'USER': 'owncommerce',
        'PASSWORD': 'camaleon',
        'HOST': 'localhost',
        'PORT': '',
    }
}

SITE_URL = "http://www.joanshop.com.ar/"

DEFAULT_FROM_EMAIL = 'testnubiquo@gmail.com'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = DEFAULT_FROM_EMAIL
EMAIL_HOST_PASSWORD = 'nubiquo1234567890'
EMAIL_USE_TLS = True


TIME_ZONE = 'America/Argentina/Cordoba'
LANGUAGE_CODE = 'es'


from celery.schedules import crontab
from datetime import timedelta
# import djcelery
# djcelery.setup_loader()

BACKUP_FOLDER = os.path.join(PROJECT_ROOT, 'backups')

CELERYBEAT_SCHEDULE = {
    'backup': {
        'task': 'owncommerce.tasks.backup',
        'schedule': timedelta(seconds=5),
        # 'schedule': crontab(minute=0, hour=0),

    },
}

CELERY_TIMEZONE = 'UTC'