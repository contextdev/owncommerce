from __future__ import unicode_literals

from django import forms
from django.core.exceptions import ObjectDoesNotExist, NON_FIELD_ERRORS
from django.forms.formsets import BaseFormSet, DEFAULT_MAX_NUM
from django.utils.translation import pgettext_lazy, ugettext_lazy
from satchless.item import InsufficientStock


class QuantityField(forms.IntegerField):

    def __init__(self, **kwargs):
        super(QuantityField, self).__init__(min_value=0, max_value=999,
                                            initial=1, **kwargs)


class AddToCartForm(forms.Form):
    """
    Class use product and cart instance.
    """
    quantity = QuantityField(label=pgettext_lazy('Form field', 'Quantity'))
    extra_data = forms.CharField(required=False, initial="", max_length=40)
    # extra_data = forms.ChoiceField(choices=[('op1', 'op1')])

    error_messages = {
        'empty-stock': ugettext_lazy(
            'Sorry. This product is currently out of stock.'
        ),
        'variant-does-not-exists': ugettext_lazy(
            'Oops. We could not find that product.'
        ),
        'insufficient-stock': ugettext_lazy(
            'Only %(remaining)d remaining in stock.'
        )
    }

    def __init__(self, *args, **kwargs):
        self.cart = kwargs.pop('cart')
        self.product = kwargs.pop('product')
        super(AddToCartForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(AddToCartForm, self).clean()
        quantity = cleaned_data.get('quantity')
        if quantity is None:
            return cleaned_data
        try:
            product_variant = self.get_variant(cleaned_data)
        except ObjectDoesNotExist:
            msg = self.error_messages['variant-does-not-exists']
            self.add_error(NON_FIELD_ERRORS, msg)
        else:
            cart_line = self.cart.get_line(product_variant)
            used_quantity = cart_line.quantity if cart_line else 0
            new_quantity = quantity + used_quantity
            try:
                self.cart.check_quantity(
                    product_variant, new_quantity, None)
            except InsufficientStock as e:
                remaining = e.item.get_stock_quantity() - used_quantity
                if remaining:
                    msg = self.error_messages['insufficient-stock']
                else:
                    msg = self.error_messages['empty-stock']
                self.add_error('quantity', msg % {'remaining': remaining})
        return cleaned_data

    def save(self):
        """
        Adds CartLine into the Cart instance.
        """
        product_variant = self.get_variant(self.cleaned_data)
        
        extra_data = self.cleaned_data.get('extra_data', "")
        # return self.cart.add(product_variant, self.cleaned_data['quantity'])
        return self.cart.add(product_variant, self.cleaned_data['quantity'], data=extra_data)

    def get_variant(self, cleaned_data):
        raise NotImplementedError()

    def add_error(self, name, value):
        errors = self.errors.setdefault(name, self.error_class())
        errors.append(value)


class ReplaceCartLineForm(AddToCartForm):
    """
    Replaces quantity in CartLine.
    """

    def __init__(self, *args, **kwargs):
        extra_data = kwargs.pop('extra_data', None)
        super(ReplaceCartLineForm, self).__init__(*args, **kwargs)
        self.fields['extra_data'].widget = forms.HiddenInput()
        self.cart_line = self.cart.get_line(self.product, extra_data)
        #self.fields['extra_data'].initial = extra_data
        self.fields['quantity'].widget.attrs = {
            'min': 1, 'max': self.product.get_stock_quantity()}

    def clean_quantity(self):
        quantity = self.cleaned_data['quantity']
        try:
            self.cart.check_quantity(self.product, quantity, None)
        except InsufficientStock as e:
            msg = self.error_messages['insufficient-stock']
            raise forms.ValidationError(msg % {'remaining': e.item.get_stock_quantity()})
        return quantity

    def clean(self):
        return super(AddToCartForm, self).clean()

    def get_variant(self, cleaned_data):
        """In cart formset product is already variant"""
        return self.product

    def save(self):
        """
        Replace quantity.
        """
        return self.cart.add(self.product, self.cleaned_data['quantity'], data=self.cleaned_data['extra_data'], replace=True)


class ReplaceCartLineFormSet(BaseFormSet):
    """
    Formset for all CartLines in the cart instance.
    """
    absolute_max = 9999
    can_delete = False
    can_order = False
    extra = 0
    form = ReplaceCartLineForm
    max_num = DEFAULT_MAX_NUM
    validate_max = False
    min_num = 0
    validate_min = False

    def __init__(self, *args, **kwargs):
        self.cart = kwargs.pop('cart')
		# TODO: Which is needed? data or extra_data?
        kwargs['initial'] = [{'quantity': cart_line.get_quantity(),
                              'data': cart_line.data,
                              'extra_data': cart_line.data,}
                             for cart_line in self.cart
                             if cart_line.get_quantity()]
        # kwargs['initial'] = [{'quantity': 1, 'data': 'hardcoded'}, {'quantity': 1, 'data': 'hardcoded'}]

        super(ReplaceCartLineFormSet, self).__init__(*args, **kwargs)

    def _construct_form(self, i, **kwargs):
        kwargs['cart'] = self.cart
        kwargs['product'] = self.cart[i].product

        #extra_data = ""
        #from option.models import Option
        #for option_set, option in ast.literal_eval(self.cart[i].data):
        #    opt = Option.objects.get(id=option)
        #    extra_data += str(opt.parent.title) + ": " + str(opt.title) + "\n"

        #print(self.cart[i].data)
        #print("EXTRA DATA")
        #print(extra_data)

        # kwargs['extra_data'] = extra_data #self.cart[i].data
        kwargs['extra_data'] = self.cart[i].data
        # kwargs['data'] = self.cart[i].data

        return super(ReplaceCartLineFormSet, self)._construct_form(i, **kwargs)

    def save(self):
        for form in self.forms:
            form.save()
