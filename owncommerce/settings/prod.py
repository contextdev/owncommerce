from .base import *


DEBUG = False

ALLOWED_HOSTS = ["*"]

#loaders = [('django.template.loaders.cached.Loader', loaders)]
#TEMPLATES[0]['OPTIONS']['loaders'] = loaders

ADMINS = (
    ('Context Informatic', 'cotextinformatic@gmail.com'),
)

MANAGERS = ADMINS


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': '127.0.0.1:11211',
        'VERSION': 1,
    }
}

SITE_DEFAULT_DATA = {
    "logo": "/static/image/logo_context.png",
    "name": "TiendaNet",
    "display_name": "TiendaNet",
    "site_url": "http://tienda.context.com.ar",
    "slogan": "Una Tienda Online para Negocios como el tuyo",
    "contact_email": "contacto@context.com.ar",
    "skype": "",
    "phones": ["+54 9 351 6222930",],
    "emails": ["contacto@context.com.ar",]
}



