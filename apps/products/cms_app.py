from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
from .menu import ProductSubMenu

class ProductsApp(CMSApp):

     name = _("Products App")  # give your app a name, this is required
     urls = ["apps.products.urls"]  # link your app to url configuration(s)
     app_name = 'product'
     #menus = [ProductSubMenu, ]

apphook_pool.register(ProductsApp)  # register your app
