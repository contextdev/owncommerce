import io
from django.template.base import TemplateDoesNotExist
from django.template.loader import base
from django.utils._os import safe_join
from django.core.exceptions import SuspiciousFileOperation
from .models import Theme
from django.core.exceptions import ObjectDoesNotExist

class Loader(base.Loader):
    is_usable = True

    def get_template_sources(self, template_name, template_dirs=None):

        for template_dir in template_dirs:
            try:
                yield safe_join(template_dir, template_name)
            except SuspiciousFileOperation:
                # The joined path was located outside of this template_dir
                # (it might be inside another one, so this isn't fatal).
                pass

    def load_template_source(self, template_name, template_dirs=None):
        template_dirs = []
        try:
            theme = Theme.objects.get(active=True)
            template_dirs.append(theme.path)
        except ObjectDoesNotExist:
            pass

        for filepath in self.get_template_sources(template_name, template_dirs):
            try:
                with io.open(filepath, encoding=self.engine.file_charset) as fp:
                    return fp.read(), filepath
            except IOError:
                pass
        raise TemplateDoesNotExist(template_name)
