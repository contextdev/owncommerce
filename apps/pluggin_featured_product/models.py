from django.db import models

from cms.models import CMSPlugin
from saleor.product.models import Product

QUERY_CHOICES = (
    ('favorite', 'Favorites'),
    ('seller', 'Seller'),
    ('featured', 'Featured')
)

LAYOUT_CHOICES = (
    ('h', 'Horizontal'),
    ('v', 'Vertical')
)

class FeaturedProductPlugin(CMSPlugin):
    """ Defined Plugin to show featured product, with opcion on queryset """
    title = models.CharField(max_length=40)
    description = models.TextField()
    # TODO: Query dinamicas? o opciones que representen query?
    layout = models.CharField(choices=LAYOUT_CHOICES, max_length=10, default='h')
    query = models.CharField(choices=QUERY_CHOICES, max_length=40)

    def __str__(self):
        return self.title
