from django.db import models

from cms.models import CMSPlugin

from apps.option.models import OptionSet

QUERY_CHOICES = (
    ('favorite', 'Favorites'),
    ('seller', 'Seller'),
    ('featured', 'Featured')
)

LAYOUT_CHOICES = (
    ('Image', 'Images'),
    ('Text', 'Text')
)

class OptionPlugin(CMSPlugin):
    """ Defined Plugin to show featured product, with opcion on queryset """
    layout = models.CharField(choices=LAYOUT_CHOICES, max_length=10, default='Image')
    option_set = models.ForeignKey(OptionSet, related_name='plugins')

    def __str__(self):
        return self.option_set.title
