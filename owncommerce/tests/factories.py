import factory
from prices import Price
from django.conf import settings
from saleor.product.models import Product, ProductVariant, Stock


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'userprofile.user'
        django_get_or_create = ('email',)


# @factory.django.mute_signals(signals.pre_save, signals.post_save)
class ProductFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'product.Product'
        django_get_or_create = ('name',)

    # categories =
    price = Price(100, currency=settings.DEFAULT_CURRENCY)
    weight = 100
    name = factory.Sequence(lambda n: 'producto-%d' % n)
    description = 'description'


class StockFactory(factory.DjangoModelFactory):
    class Meta:
        model = Stock

    location = 'location'
    # variant = None
    # variant = factory.SubFactory(ProductVariantFactory)


class ProductVariantFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'product.ProductVariant'

    sku = factory.Sequence(lambda n: 'sku-%d' % n)
    name = factory.Sequence(lambda n: 'variant-%d' % n)
    price_override = Price(150, currency=settings.DEFAULT_CURRENCY)
    product = factory.SubFactory(ProductFactory)

