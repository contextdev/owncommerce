from __future__ import unicode_literals

from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.utils.http import is_safe_url
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.http import require_http_methods
from django.views.generic import ListView


from django.views.generic import DetailView
from django.utils.decorators import method_decorator

from apps.option.models import OptionSet, Option


from ...product.models import Product, ProductImage, Stock, ProductAttribute, \
    ProductVariant
from ..utils import paginate
from ..views import staff_member_required
from . import forms
from django.core.urlresolvers import reverse
from .forms import StockFilterForm
from ..views import (StaffMemberOnlyMixin, staff_member_required)


@staff_member_required
def product_list(request):
    products = Product.objects.prefetch_related('images').select_subclasses()
    form = forms.ProductClassForm(request.POST or None)
    if form.is_valid():
        return redirect('dashboard:product-add')
    products, paginator = paginate(products, 30, request.GET.get('page'))
    ctx = {'form': form, 'products': products, 'paginator': paginator}
    return TemplateResponse(request, 'dashboard/product/list.html', ctx)



from django.forms.models import inlineformset_factory
ProductImage_inline_formset = inlineformset_factory(
    Product, ProductImage, extra=2, can_delete=True, exclude=('alt', ))

@staff_member_required
def product_create(request):
    product = Product()
    form = forms.ProductForm(request.POST or None, instance=product)
    form_image = ProductImage_inline_formset(request.POST or None, request.FILES or None, instance=form.instance)
    if form.is_valid():
        product = form.save()
        msg = _('Added product %s') % product
        messages.success(request, msg)

        form_image = ProductImage_inline_formset(request.POST or None, request.FILES or None, instance=product)
        if form_image.is_valid():
            form_image.save()
        return redirect('dashboard:variant-add', product_pk=product.pk)
    ctx = {'product_form': form, 'product': product, 'images_form': form_image }
    return TemplateResponse(request, 'dashboard/product/product_form.html', ctx)


@staff_member_required
def product_edit(request, pk):
    product = get_object_or_404(
        Product.objects.select_subclasses().prefetch_related('images',
                                                             'variants'), pk=pk)
    attributes = product.attributes.prefetch_related('values')
    images = product.images.all()
    variants = product.variants.select_subclasses()
    stock_items = Stock.objects.filter(variant__in=variants)

    form = forms.ProductForm(request.POST or None, instance=product)
    variants_delete_form = forms.VariantBulkDeleteForm()
    stock_delete_form = forms.StockBulkDeleteForm()

    if form.is_valid():
        product = form.save()
        msg = _('Updated product %s') % product
        messages.success(request, msg)
        return redirect('dashboard:product-update', pk=product.pk)
    ctx = {'attributes': attributes, 'images': images, 'product_form': form,
           'product': product, 'stock_delete_form': stock_delete_form,
           'stock_items': stock_items, 'variants': variants,
           'variants_delete_form': variants_delete_form}
    return TemplateResponse(request, 'dashboard/product/product_form.html', ctx)


@staff_member_required
def product_delete(request, pk):
    product = get_object_or_404(Product, pk=pk)
    if request.method == 'POST':
        product.delete()
        messages.success(request, _('Deleted product %s') % product)
        return redirect('dashboard:product-list')
    return TemplateResponse(
        request, 'dashboard/product/modal_product_confirm_delete.html',
        {'product': product})


@staff_member_required
def stock_edit(request, product_pk, stock_pk=None):
    product = get_object_or_404(Product, pk=product_pk)
    if stock_pk:
        stock = get_object_or_404(Stock, pk=stock_pk)
    else:
        stock = Stock()
    form = forms.StockForm(request.POST or None, instance=stock,
                           product=product)
    if form.is_valid():
        form.save()
        messages.success(request, _('Saved stock'))
        success_url = request.POST['success_url']
        if is_safe_url(success_url, request.get_host()):
            return redirect(success_url)
    else:
        print("invalid Form")
    ctx = {'form': form, 'product': product, 'stock': stock}
    return TemplateResponse(request, 'dashboard/product/stock_form.html', ctx)


@staff_member_required
def stock_delete(request, product_pk, stock_pk):
    product = get_object_or_404(Product, pk=product_pk)
    stock = get_object_or_404(Stock, pk=stock_pk)
    if request.method == 'POST':
        stock.delete()
        messages.success(request, _('Deleted stock'))
        success_url = request.POST['success_url']
        if is_safe_url(success_url, request.get_host()):
            return redirect(success_url)
    ctx = {'product': product, 'stock': stock}
    return TemplateResponse(
        request, 'dashboard/product/stock_confirm_delete.html', ctx)


@staff_member_required
@require_http_methods(['POST'])
def stock_bulk_delete(request, product_pk):
    product = get_object_or_404(Product, pk=product_pk)
    form = forms.StockBulkDeleteForm(request.POST)
    if form.is_valid():
        form.delete()
        success_url = request.POST['success_url']
        messages.success(request, _('Deleted stock'))
        if is_safe_url(success_url, request.get_host()):
            return redirect(success_url)
    return redirect('dashboard:product-update', pk=product.pk)


@staff_member_required
def product_image_edit(request, product_pk, img_pk=None):
    product = get_object_or_404(Product, pk=product_pk)
    if img_pk:
        product_image = get_object_or_404(product.images, pk=img_pk)
    else:
        product_image = ProductImage(product=product)
    form = forms.ProductImageForm(request.POST or None, request.FILES or None,
                                  instance=product_image)
    if form.is_valid():
        product_image = form.save()
        if img_pk:
            msg = _('Updated image %s') % product_image.image.name
        else:
            msg = _('Added image %s') % product_image.image.name
        messages.success(request, msg)
        success_url = request.POST['success_url']
        if is_safe_url(success_url, request.get_host()):
            return redirect(success_url)
    ctx = {'form': form, 'product': product, 'product_image': product_image}
    return TemplateResponse(
        request, 'dashboard/product/product_image_form.html', ctx)


@staff_member_required
def product_image_delete(request, product_pk, img_pk):
    product = get_object_or_404(Product, pk=product_pk)
    product_image = get_object_or_404(product.images, pk=img_pk)
    if request.method == 'POST':
        product_image.delete()
        messages.success(
            request, _('Deleted image %s') % product_image.image.name)
        success_url = request.POST['success_url']
        if is_safe_url(success_url, request.get_host()):
            return redirect(success_url)
    ctx = {'product': product, 'product_image': product_image}
    return TemplateResponse(
        request,
        'dashboard/product/modal_product_image_confirm_delete.html', ctx)


@staff_member_required
def variant_edit(request, product_pk, variant_pk=None):
    product = get_object_or_404(Product.objects.select_subclasses(),
                                pk=product_pk)
    form_initial = {}
    if variant_pk:
        variant = get_object_or_404(product.variants.select_subclasses(),
                                    pk=variant_pk)
    else:
        #if len(product.attributes.all()) > 0:
        #    print("EL PRODUCTO TIENE ATTRIBUTOS")
        variant = ProductVariant(product=product)
    form = forms.ProductVariantForm(request.POST or None, instance=variant,
                                    initial=form_initial)
    attribute_form = forms.VariantAttributeForm(request.POST or None,
                                                instance=variant)
    if all([form.is_valid(), attribute_form.is_valid()]):
        variant = form.save()
        attribute_form.save()
        if variant_pk:
            msg = _('Updated variant %s') % variant.name
        else:
            msg = _('Added variant %s') % variant.name
        messages.success(request, msg)
        success_url = request.POST['success_url']
        stock = Stock()
        stock.variant = variant
        stock.save()
        if is_safe_url(success_url, request.get_host()):
            return redirect(success_url)
    ctx = {'attribute_form': attribute_form, 'form': form, 'product': product,
           'variant': variant}
    return TemplateResponse(request, 'dashboard/product/variant_form.html', ctx)


@staff_member_required
def variant_delete(request, product_pk, variant_pk):
    product = get_object_or_404(Product, pk=product_pk)
    variant = get_object_or_404(product.variants, pk=variant_pk)
    is_only_variant = product.variants.count() == 1
    if request.method == 'POST':
        variant.delete()
        messages.success(request, _('Deleted variant %s') % variant.name)
        success_url = request.POST['success_url']
        if is_safe_url(success_url, request.get_host()):
            return redirect(success_url)
    ctx = {'is_only_variant': is_only_variant, 'product': product,
           'variant': variant}
    return TemplateResponse(
        request,
        'dashboard/product/modal_product_variant_confirm_delete.html', ctx)


@staff_member_required
@require_http_methods(['POST'])
def variants_bulk_delete(request, product_pk):
    product = get_object_or_404(Product, pk=product_pk)
    form = forms.VariantBulkDeleteForm(request.POST)
    if form.is_valid():
        form.delete()
        success_url = request.POST['success_url']
        messages.success(request, _('Deleted variants'))
        if is_safe_url(success_url, request.get_host()):
            return redirect(success_url)
    return redirect('dashboard:product-update', pk=product.pk)


@staff_member_required
def attribute_list(request):
    attributes = ProductAttribute.objects.prefetch_related('values')
    ctx = {'attributes': attributes}
    return TemplateResponse(request, 'dashboard/product/attributes/list.html',
                            ctx)


@staff_member_required
def attribute_edit(request, pk=None):
    if pk:
        attribute = get_object_or_404(ProductAttribute, pk=pk)
    else:
        attribute = ProductAttribute()
    form = forms.ProductAttributeForm(request.POST or None, instance=attribute)
    formset = forms.AttributeChoiceValueFormset(request.POST or None,
                                                request.FILES or None,
                                                instance=attribute)
    if all([form.is_valid(), formset.is_valid()]):
        attribute = form.save()
        formset.save()
        msg = _('Updated attribute') if pk else _('Added attribute')
        messages.success(request, msg)
        return redirect('dashboard:product-attribute-update', pk=attribute.pk)
    ctx = {'attribute': attribute, 'form': form, 'formset': formset}
    return TemplateResponse(request, 'dashboard/product/attributes/form.html',
                            ctx)


@staff_member_required
def attribute_delete(request, pk):
    attribute = get_object_or_404(ProductAttribute, pk=pk)
    if request.method == 'POST':
        attribute.delete()
        messages.success(request, _('Deleted attribute %s' % attribute.display))
        return redirect('dashboard:product-attributes')
    ctx = {'attribute': attribute}
    return TemplateResponse(
        request, 'dashboard/product/attributes/modal_confirm_delete.html', ctx)


@staff_member_required
def optionset_list(request):
    options = OptionSet.objects.prefetch_related('options')
    ctx = {'options': options}
    return TemplateResponse(request, 'dashboard/product/option/list.html',
                            ctx)


class ProductOptionDetail(DetailView):
    model = OptionSet
    template_name = 'dashboard/product/option/detail.html'
    queryset = OptionSet.objects.all()

    @method_decorator(staff_member_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductOptionDetail, self).dispatch(request, *args, **kwargs)


@staff_member_required
def optionset_form(request, pk=None):
    if pk:
        option = get_object_or_404(OptionSet, pk=pk)
    else:
        option = OptionSet()

    form = forms.OptionSetForm(request.POST or None, request.FILES or None, instance=option)

    if form.is_valid():
        option_set = form.save()
        msg = _('Updated options') if pk else _('Added options')
        messages.success(request, msg)
        return redirect('dashboard:product-option-set-detail', pk=option_set.pk)

    ctx = {'option': option, 'form': form}
    return TemplateResponse(request, 'dashboard/product/option/form.html',
                            ctx)

@staff_member_required
def optionset_delete(request, pk):
    optionSet = get_object_or_404(OptionSet, pk=pk)
    if request.method == 'POST':
        # TODO: Move validation if has plugins on models
        if len(optionSet.plugins.all()) > 0:
            messages.error(request, _('No se puede eliminar esta opcion set, ya que tiene plugins asociados.'))
        else:
            optionSet.delete()
            messages.success(request, _('Deleted option %s' % optionSet.title))

        return redirect('dashboard:product-options-set')
    ctx = {'optionSet': optionSet}
    return TemplateResponse(
        request, 'dashboard/product/option/modal_confirm_delete.html', ctx)


@staff_member_required
def option_form(request, pk=None, pk_parent=None):
    if pk_parent:
        if pk:
            option = get_object_or_404(Option, pk=pk)
        else:
            option = Option()

        form = forms.OptionForm(request.POST or None, request.FILES or None, instance=option)

        if form.is_valid():
            form.save(parent=get_object_or_404(OptionSet, pk=pk_parent))
            msg = _('Updated options') if pk else _('Added options')
            messages.success(request, msg)
            return redirect('dashboard:product-option-set-detail', pk=pk_parent)

        ctx = {'option': option, 'form': form}
        return TemplateResponse(request, 'dashboard/product/option/option_form.html',
                                ctx)

    messages.error(request, "Error!!")
    return redirect('dashboard:product-options-set')


def option_delete(request, pk, pk_parent):
    option = get_object_or_404(Option, pk=pk)
    if request.method == 'POST':
        option.delete()
        messages.success(request, _('Deleted option %s' % option.title))

        return redirect('dashboard:product-option-set-detail', pk=pk_parent)
    ctx = {'option': option}
    return TemplateResponse(
        request, 'dashboard/product/option/option_modal_confirm_delete.html', ctx)


def input_output_stock(request):
    #ioStock_form = forms.InputOutputStockForm(request.POST or None)
    #or this block
    if request.method == 'POST':
        ioStock_form = forms.InputOutputStockForm(request.POST or None)
    else:
        variant_pk = request.GET.get('variant')
        try:
            if variant_pk:
                ioStock_form = forms.InputOutputStockForm(initial={
                                    'variation_hidden': variant_pk,
                                    'product': ProductVariant.objects.get(id=variant_pk).product.id
                                })
            else:
                ioStock_form = forms.InputOutputStockForm()
        except ProductVariant.DoesNotExist:
            ioStock_form = forms.InputOutputStockForm()


    if ioStock_form.is_valid():
        ioStock = ioStock_form.save()
        msg = _('Updated stock to product %s') % ioStock
        messages.success(request, msg)
        callback_url = request.GET.get('callback_url')
        if callback_url:
            return redirect(callback_url)

    ctx = { 'form': ioStock_form}
    return TemplateResponse(request, 'dashboard/product/iostock_form.html', ctx)



import django_filters

class StockFilter(django_filters.FilterSet):
    class Meta:
        model = Stock
        fields = {
            'variant__product': ['exact'],
            }
        # order_by = ['variant__product', '-variant__product', 'quantity', '-quantity']


class StockListView(StaffMemberOnlyMixin, ListView):
    template_name = 'dashboard/stock/list.html'
    paginate_by = 20
    model = Stock

    def get_queryset(self):
        qs = StockFilter(self.request.GET, queryset=Stock.objects.all())
        return qs

    def get_context_data(self, **kwargs):
        context = super(StockListView, self).get_context_data(**kwargs)
        context['form'] = StockFilterForm()
        return context
