# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from decimal import Decimal
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0002_auto_20150926_1813'),
    ]

    operations = [
        migrations.CreateModel(
            name='InputOutputStock',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('quantity', models.IntegerField()),
                ('note', models.CharField(max_length=255, blank=True)),
                ('date', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.AlterField(
            model_name='stock',
            name='location',
            field=models.CharField(max_length=100, blank=True, null=True, verbose_name='location'),
        ),
        migrations.AlterField(
            model_name='stock',
            name='quantity',
            field=models.IntegerField(verbose_name='quantity', validators=[django.core.validators.MinValueValidator(0)], default=Decimal('0')),
        ),
        migrations.AlterUniqueTogether(
            name='stock',
            unique_together=set([]),
        ),
        migrations.AddField(
            model_name='inputoutputstock',
            name='stock',
            field=models.ForeignKey(related_name='stock_history', to='product.Stock'),
        ),
    ]
