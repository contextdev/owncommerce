
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import FeaturedProductPlugin
from django.utils.translation import ugettext as _
from saleor.product.models import Product
from favit.models import Favorite
from django.db.models import Count

class CMSFeaturedProductPlugin(CMSPluginBase):
    """
        Plugin to show products in carousel
    """
    model = FeaturedProductPlugin  # model where plugin data are saved
    module = _("Products")
    name = _("Product Plugin")  # name of the plugin in the interface
    render_template = "product/_featured_product.html"

    def render(self, context, instance, placeholder):
        if instance.query == 'favorite':
            objs_fav = Favorite.objects.for_model(Product).annotate(num_fav=Count('target_object_id')).order_by('-num_fav').values_list('target_object_id')[:8]
            products = Product.objects.filter(id__in=objs_fav).prefetch_related('images')[:10]
        elif instance.query == 'featured':
            products = Product.objects.all().filter(outstanding=True).prefetch_related('images')[:10]
        elif instance.query == 'seller':
            products = Product.objects.all()
        else:
            products = Product.objects.all()[:6]
        context.update({'featured_product_title': instance.title, 'featured_product_description': instance.description, 'featured_product_list': products })
        return context

    def _get_render_template(self, context, instance, placeholder):
        if instance.layout == 'v':
            return 'product/_v_featured_product.html'

        return super(CMSFeaturedProductPlugin, self)._get_render_template(context, instance, placeholder)

plugin_pool.register_plugin(CMSFeaturedProductPlugin)  # register the plugin
