# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CustomCss',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('selector', models.CharField(max_length=60)),
                ('display_name', models.CharField(max_length=60)),
            ],
        ),
        migrations.CreateModel(
            name='CustomCssAttribute',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('attr_name', models.CharField(max_length=60)),
                ('value', models.CharField(blank=True, null=True, max_length=120)),
                ('selector', models.ForeignKey(to='custom_theme.CustomCss', related_name='attributes')),
            ],
        ),
        migrations.CreateModel(
            name='Theme',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=20)),
                ('path', models.CharField(max_length=60)),
                ('active', models.BooleanField()),
            ],
        ),
    ]
